package it.com.atlassian.bitbucket.server.suggestreviewers.rest;

import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import com.jayway.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.UUID;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.*;
import static org.hamcrest.Matchers.equalTo;

public class SuggestedReviewersResourceTest extends BaseRetryingFuncTest {

    @BeforeClass
    public static void setupUser() {
        //This user gives the suggestion code someone to suggest. Their e-mail address needs to match the
        //commit's author, and they need to have REPO_READ access to the repository
        getClassTestContext()
                .user("mheemskerk", "Michael Heemskerk", UUID.randomUUID().toString(),
                        "mheemskerk@atlassian.com", true)
                .repositoryPermission(getProject1(), getProject1Repository1(), "mheemskerk", Permission.REPO_READ);
    }

    @Test
    public void testByRef() {
        runSuggestionTest("ref", "refs/heads/basic_branching", "refs/heads/master");
    }

    @Test
    public void testBySha() {
        runSuggestionTest("sha", "d6edcbf924697ab811a867421dab60d954ccad99",
                "0a943a29376f2336b78312d99e65da17048951db");
    }

    @Test
    public void testBadFromRef() {
        getSuggestionsBadRequest("sha", "", "0a943a29376f2336b78312d99e65da17048951db");
        getSuggestionsBadRequest("ref", "", "refs/heads/master");
    }

    @Test
    public void testBadToRef() {
        getSuggestionsBadRequest("sha", "d6edcbf924697ab811a867421dab60d954ccad99", "");
        getSuggestionsBadRequest("ref", "refs/heads/basic_branching", "");
    }

    private static void getSuggestionsBadRequest(String by, String from, String to) {
        RestAssured.given()
                .auth().preemptive().basic(getAdminUser(), getAdminPassword())
                .expect()
                .statusCode(400)
                .log().ifValidationFails()
                .when()
                .get(getRestURL("suggest-reviewers", "1.0") + "/by/" + by +
                     "?fromRepoId=1&from=" + from + "&toRepoId=1&to=" + to);
    }

    private static void runSuggestionTest(String by, String from, String to) {
        RestAssured.given()
                .auth().preemptive().basic(getAdminUser(), getAdminPassword())
                .expect()
                .statusCode(200)
                .body("size()", equalTo(1))
                .body("[0].reviewingCount", equalTo(0))
                .body("[0].shortReason", equalTo("Authored 1 commit to be merged."))
                .body("[0].user.name", equalTo("mheemskerk"))
                .log().ifValidationFails()
                .when()
                .get(getRestURL("suggest-reviewers", "1.0") + "/by/" + by +
                        "?fromRepoId=1&from=" + from + "&toRepoId=1&to=" + to);
    }
}
