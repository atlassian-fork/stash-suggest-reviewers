package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.user.ApplicationUser;

import java.util.Objects;

public class RankedUser implements Comparable<RankedUser> {

    private final ApplicationUser user;
    private int score;

    public RankedUser(ApplicationUser user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof RankedUser)) {
            return false;
        }
        return compareTo((RankedUser) o) == 0;
    }

    public ApplicationUser getUser() {
        return user;
    }

    public void add(int score) {
        this.score += score;
    }

    @Override
    public int compareTo(RankedUser other) {
        return other.score - score;
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, score);
    }
}
