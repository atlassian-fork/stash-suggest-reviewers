package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.pull.*;
import com.atlassian.bitbucket.user.ApplicationUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SuggestedReviewerMetadataProviderTest {

    @Mock
    PullRequestService pullRequestService;

    @InjectMocks
    SuggestedReviewerMetadataProvider metadataProvider;

    @Test
    public void testNullUserReturnsZero() {
        assertEquals(0, metadataProvider.getTotalReviewingCount(null));
    }

    @Test
    public void testSearchRequests() throws Exception {
        ApplicationUser user = mock(ApplicationUser.class);
        when(user.getName()).thenReturn("jbourne");
        ArgumentCaptor<PullRequestSearchRequest> captor = ArgumentCaptor.forClass(PullRequestSearchRequest.class);
        when(pullRequestService.count(captor.capture())).thenReturn(Long.valueOf(2));

        assertEquals(2, metadataProvider.getTotalReviewingCount(user));
        assertEquals("Only searching for open pull requests.", PullRequestState.OPEN, captor.getValue().getState());

        Collection<PullRequestParticipantRequest> participantRequests = captor.getValue().getParticipants();
        assertThat("There is only one participant in the search request", participantRequests, hasSize(1));

        PullRequestParticipantRequest participantRequest = participantRequests.stream().findFirst().get();
        assertThat("The participant is a reviewer", participantRequest.getRole(), equalTo(PullRequestRole.REVIEWER));
        assertThat("The participant has not approved.", participantRequest.getStatuses(), not(contains(PullRequestParticipantStatus.APPROVED)));
    }

}
