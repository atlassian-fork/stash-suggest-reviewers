package it.com.atlassian.bitbucket.server.suggestreviewers.rest;

import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.bitbucket.BitbucketTestedProduct;
import com.atlassian.webdriver.bitbucket.page.BitbucketLoginPage;
import com.atlassian.webdriver.bitbucket.page.PullRequestCreatePage;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.List;
import java.util.UUID;
import java.util.stream.StreamSupport;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.getAdminPassword;
import static com.atlassian.bitbucket.test.DefaultFuncTestData.getAdminUser;
import static com.atlassian.bitbucket.test.DefaultFuncTestData.getProject1;
import static com.atlassian.bitbucket.test.DefaultFuncTestData.getProject1Repository1;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class SuggestedReviewersBrowserTest extends BaseRetryingFuncTest {

    protected static final BitbucketTestedProduct BITBUCKET = TestedProductFactory.create(BitbucketTestedProduct.class);

    @BeforeClass
    public static void setupClass() {
        //This user gives the suggestion code someone to suggest. Their e-mail address needs to match the
        //commit's author, and they need to have REPO_READ access to the repository
        getClassTestContext()
                .user("mheemskerk", "Michael Heemskerk", UUID.randomUUID().toString(),
                        "mheemskerk@atlassian.com", true)
                .repositoryPermission(getProject1(), getProject1Repository1(), "mheemskerk", Permission.REPO_READ);
    }

    @Test
    public void testAddSuggestedReviewer() {
        PullRequestCreatePage page = visitPullRequestCreatePage();
        page.getSourceBranchSelector().open().selectItemByName("basic_branching");
        page.clickContinue();
        waitUntilLoaded(page);

        assertThat(getReviewers(page), empty());

        List<PageElement> suggestedReviewers = page.getDetails().findAll(By.className("add-suggested-reviewer"));
        assertThat(suggestedReviewers.size(), equalTo(1));
        PageElement reviewer = suggestedReviewers.get(0);
        assertThat(reviewer.getText(), equalTo("Michael Heemskerk (0)"));

        reviewer.click();

        List<String> reviewers = getReviewers(page);
        assertThat(reviewers, contains("mheemskerk"));
    }

    private static List<String> getReviewers(PullRequestCreatePage page) {
        return StreamSupport.stream(page.getDetails().getReviewerUsernames().spliterator(), false).collect(toList());
    }

    private static PullRequestCreatePage visitPullRequestCreatePage() {
        return BITBUCKET.visit(BitbucketLoginPage.class).login(getAdminUser(), getAdminPassword(), PullRequestCreatePage.class, getProject1(), getProject1Repository1());
    }

    private static void waitUntilLoaded(PullRequestCreatePage page) {
        waitUntilFalse(page.getDetails().find(By.className("description-spinner")).timed().isVisible());
    }
}
