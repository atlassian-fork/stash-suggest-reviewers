package com.atlassian.bitbucket.server.suggestreviewers;

import com.atlassian.bitbucket.user.ApplicationUser;

import javax.annotation.Nonnull;

/**
 * A {@link ApplicationUser} who has been suggested as a reviewer.
 *
 * @since 1.0
 */
public interface SuggestedReviewer {

    /**
     * @return the {@link ApplicationUser}
     */
    @Nonnull
    ApplicationUser getUser();

    /**
     * @return a short description of the primary reason the user was suggested as a reviewer.
     */
    @Nonnull
    String getShortReason();

    /**
     * <strong>This is not displayed to the user in version 1.0, but may be in a future release.</strong>
     *
     * @return longer descriptions of all the reasons the user was suggested as a reviewer.
     */
    @Nonnull
    Iterable<String> getReasons();

    /**
     * @return the total number of pull requests this user is currently reviewing.
     */
    @Nonnull
    Integer getReviewingCount();

}
