# Bitbucket Server Suggest Reviewers plugin

The Bitbucket Server Suggest Reviewers plugin provides intelligent reviewer suggestions for Pull Requests.

![Pull Requests](docs/suggestions.png)

## How it works

Reviewers are suggested by mapping the email addresses of repository contributors to users. The email set in your
[local git config](http://git-scm.com/book/en/Getting-Started-First-Time-Git-Setup#Your-Identity) must match the email
address you have registered in Bitbucket for you to be suggested as a reviewer.

Reviewers that are more likely to be relevant are suggested first. By default, reviewers are suggested using two
different algorithms:

- **Contribution**: authors who contributed commits to the pull request are ranked very highly in the suggestions.
- **Blame**: authors who have previously contributed code to files that are modified by the pull request are also
  suggested. The more they have contributed, the higher their ranking in the suggestions.

The algorithms used to suggest reviewers are a plugin point, so you can add your own reviewer suggestions through a
plugin.

## Adding your own suggestion algorithms

You will need to create a Stash plugin to add your own suggestion algorithms, here's how to do that:

- Install the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Getting+Started).
- Create a [new Bitbucket Server plugin](https://developer.atlassian.com/bitbucket/server/docs/latest/how-tos/creating-a-bitbucket-server-plugin.html).
- Add a dependency on the Suggest Reviewers plugin to your plugin's `pom.xml`:

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server.plugin</groupId>
    <artifactId>bitbucket-suggest-reviewers</artifactId>
    <version>3.0.0</version>
</dependency>
```

- Create a new class that implements the [ReviewerSuggester](src/main/java/com/atlassian/bitbucket/server/suggestreviewers/spi/ReviewerSuggester.java) interface.
- Register the interface by adding the following to your plugin's `atlassian-plugin.xml`:

```xml
<reviewer-suggester key="a-key-for-your-reviewer-suggester" 
                    name="a-name-for-your-reviewers-suggester"
                    class="com.yourcompany.your.SuggesterImplementation" />
```

More information about Bitbucket Server plugin development is available in the [Bitbucket Server Developer Docs](https://developer.atlassian.com/bitbucket/server/docs/latest/).
