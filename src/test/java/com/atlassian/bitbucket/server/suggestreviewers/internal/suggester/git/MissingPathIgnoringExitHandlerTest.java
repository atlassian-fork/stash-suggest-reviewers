package com.atlassian.bitbucket.server.suggestreviewers.internal.suggester.git;

import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.repository.Repository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MissingPathIgnoringExitHandlerTest {

    private static final String COMMAND = "command";
    @Mock
    Repository repository;

    @Mock
    I18nService i18nService;

    @Spy
    @InjectMocks
    MissingPathIgnoringExitHandler exitHandler;

    @Test
    public void testOnError() {
        exitHandler.onError(COMMAND, 1, "fatal: no such path bla bla bla", new IllegalStateException());

        verifyZeroInteractions(repository);
        verifyZeroInteractions(i18nService);
        verify(exitHandler, never()).superOnError(anyString(), anyInt(), anyString(), any());
    }

    @Test
    public void testOnErrorCallsSuper() {
        String ERR = "fatal: something something";
        int exitCode = 1;
        doNothing().when(exitHandler).superOnError(eq(COMMAND), eq(exitCode), eq(ERR), any());

        exitHandler.onError(COMMAND, exitCode, ERR, new IllegalStateException());

        verifyZeroInteractions(repository);
        verifyZeroInteractions(i18nService);
        verify(exitHandler).superOnError(eq(COMMAND), anyInt(), eq(ERR), any());
    }

}