package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.pull.*;
import com.atlassian.bitbucket.user.ApplicationUser;

import javax.annotation.Nullable;

/**
 * A helper service to provide additional metadata about the suggested reviewers.
 */
public class SuggestedReviewerMetadataProvider {

    private final PullRequestService pullRequestService;

    SuggestedReviewerMetadataProvider(PullRequestService pullRequestService) {
        this.pullRequestService = pullRequestService;
    }

    /**
     * @param user
     * @return the total number of pull requests this user is currently reviewing
     */
    int getTotalReviewingCount(@Nullable ApplicationUser user) {
        if (user == null) {
            return 0;
        }
        PullRequestParticipantRequest participantRequest = new PullRequestParticipantRequest.Builder(user.getName())
                .status(PullRequestParticipantStatus.NEEDS_WORK)
                .status(PullRequestParticipantStatus.UNAPPROVED)
                .role(PullRequestRole.REVIEWER)
                .build();

        PullRequestSearchRequest request = new PullRequestSearchRequest.Builder()
                .participant(participantRequest)
                .state(PullRequestState.OPEN)
                .withProperties(false)
                .build();

        return new Long(pullRequestService.count(request)).intValue();
    }
}
