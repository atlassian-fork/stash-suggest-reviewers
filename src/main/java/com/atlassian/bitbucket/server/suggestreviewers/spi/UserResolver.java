package com.atlassian.bitbucket.server.suggestreviewers.spi;

import com.atlassian.bitbucket.user.ApplicationUser;

import javax.annotation.Nullable;

/**
 * A utility for resolving {@link ApplicationUser ApplicationUsers} from email addresses.
 *
 * @since 1.0
 */
public interface UserResolver {

    /**
     * @param emailAddresses email addresses that may or may not correspond to application users.
     * @return any {@link ApplicationUser ApplicationUsers} that match the supplied email addresses. If an email address appears
     * multiple times, the resultant {@link Iterable} will contain the corresponding {@link ApplicationUser} multiple times.
     * Note that email addresses with no match will be disregarded, so the returned {@link Iterable} may contain fewer
     * elements than the one supplied.
     */
    Iterable<ApplicationUser> resolve(Iterable<String> emailAddresses);

    /**
     * @param emailAddress an email address that may or may not correspond to a application user.
     * @return the {@link ApplicationUser ApplicationUser} matching the supplied email addresses, or {@code null} if the email
     * address does not match any application user.
     */
    @Nullable
    ApplicationUser resolve(String emailAddress);

}
