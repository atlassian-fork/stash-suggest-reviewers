package com.atlassian.bitbucket.server.suggestreviewers;

import com.atlassian.bitbucket.commit.Commit;

/**
 * Suggests appropriate reviewers for a set of {@link Commit commits}.
 *
 * @since 1.0
 */
public interface SuggestedReviewerService {

    /**
     * @param pullRequestDetails
     * @param limit the number of reviewer suggestions to return
     * @return
     */
    Iterable<SuggestedReviewer> getSuggestedReviewers(PullRequestDetails pullRequestDetails, int limit);

}