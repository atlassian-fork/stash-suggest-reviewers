package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.server.suggestreviewers.spi.Reason;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Encapsulates all the logic for holding a complete user ranking, including {@link RankedUser ranked users}, the reason
 * for their ranking, along with a 'most relevant reason'.
 */
public class UserRanking {

    private final Multimap<ApplicationUser, Reason> reasons;
    private final Map<ApplicationUser, Reason> mostRelevantReason;
    private final Map<ApplicationUser, RankedUser> rankings;

    public UserRanking() {
        reasons = HashMultimap.create();
        mostRelevantReason = new HashMap<>();
        rankings = new HashMap<>();
    }

    public Reason getMostRelevantReason(ApplicationUser user) {
        return mostRelevantReason.get(user);
    }

    public Collection<Reason> getAllReasons(ApplicationUser user) {
        return reasons.get(user);
    }

    public Stream<RankedUser> getSortedRankedUserStream() {
        return rankings.values().stream().sorted();
    }

    public void addRanking(ApplicationUser user, Collection<Reason> reasons) {
        RankedUser ranking = rankings.get(user);
        if (ranking == null) {
            ranking = new RankedUser(user);
            rankings.put(user, ranking);
        }

        Reason best = mostRelevantReason.get(user);
        for (Reason reason : reasons) {
            this.reasons.put(user, reason);
            ranking.add(reason.getScore());
            if (best == null || reason.getScore() > best.getScore()) {
                best = reason;
            }
        }
        mostRelevantReason.put(user, best);
    }

}
