package com.atlassian.bitbucket.server.suggestreviewers.internal.util;

import java.util.Objects;
import java.util.function.Function;

public class IntHolder implements Comparable<IntHolder> {

    public static final Function<IntHolder, Integer> TO_INT = input -> input.n;

    private int n;

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof IntHolder)) {
            return false;
        }
        return compareTo((IntHolder) o) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(n);
    }

    public int increment() {
        return ++n;
    }

    public int decrement() {
        return --n;
    }

    public int add(int increment) {
        return n += increment;
    }

    public int get() {
        return n;
    }

    @Override
    public int compareTo(IntHolder o) {
        return n - o.n;
    }
}
